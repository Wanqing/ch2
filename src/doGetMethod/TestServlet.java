package doGetMethod;

import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/getServlet")
public class TestServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6711019587195271719L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, java.io.IOException {
		String temperature = req.getParameter("temperature");
		DecimalFormat twoDigits = new DecimalFormat("0.00");

		try {
			double tempF = Double.parseDouble(temperature);
			String tempC = twoDigits.format((tempF - 32) * 5.0 / 9.0);
			PrintWriter out = res.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h3>" + temperature + " Fahrenheit is converted to " + tempC + " Celsius</h3><p>");
			out.println("</body>");
			out.println("</html>");
		}
		catch (Exception e) {
			res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "There was an input error");
		}
	}
}

